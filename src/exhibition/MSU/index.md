---
layout: layouts/exhibition.njk
title: "And So It Goes"
venue: "Northcutt Steele Gallery at Montana State University Billings"
city: "Billings"
state: "Montana"
location: "Billings, MT"
type: "Solo"

works:
  - MSU

tags:
  - exhibition

year: "2023"
dates: "October 12 - November 9, 2023"
curator: "Robin Earles"

# link: ""

featured_image: "MSU-10.jpg"
featured_audio: ""
featured_video: ""
installation_images:
  - "MSU-1.jpg"
  - "MSU-2.jpg"
  - "MSU-3.jpg"
  - "MSU-4.jpg"
  - "MSU-5.jpg"
  - "MSU-6.jpg"
  - "MSU-7.jpg"
  - "MSU-8.jpg"
  - "MSU-9.jpg"


video: "https://iframe.mediadelivery.net/embed/21069/c1759cf6-8c17-4d3e-ae9c-abe7519c7d26?autoplay=true"
---

