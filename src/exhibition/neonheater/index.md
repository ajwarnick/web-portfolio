---
layout: layouts/exhibition.njk
title: "__________ News"
venue: "The Neon Heater"
city: "Findlay"
state: "Ohio"
location: "Findlay, OH"
type: "Solo"

works:
  - "Neon Heater"

tags:
  - exhibition

year: "2023"
dates: "November 3 - November 25, 2023"
curator: "Ian Breidenbach"

# link: ""

# participants:
#   - {
#     name: "Alicia Little",
#     link: "https://alicialittle.info/"
#   }
#   - {
#     name: "Isabelle Carr",
#     link: "https://www.wonderingpeople.com/en-us/blogs/artists/isabelle-carr"
#   }
#   - {
#     name: "Amelie Peace",
#     link: "https://ameliepeacearchive.com/"
#   }
#   - {
#     name: "Kaija Hinkula",
#     link: "https://kaijahinkula.com/"
#   }
#   - {
#     name: "Irene Anguita Cuadra"
#   }
#   - {
#     name: "Shea Chang",
#     link: "https://sheachang.com/"
#   }
#   - {
#     name: "Sharon Haward",
#     link: "https://www.sharonhaward.com/"
#   }


featured_image: "TheNeonHeater-1.jpg"
featured_audio: ""
featured_video: ""
installation_images:
  - "TheNeonHeater-1.jpg"
  - "TheNeonHeater-2.jpg"
  - "TheNeonHeater-3.jpg"
  - "TheNeonHeater-4.jpg"
  - "TheNeonHeater-5.jpg"
  - "TheNeonHeater-6.jpg"
  - "TheNeonHeater-7.jpg"
  - "TheNeonHeater-8.jpg"
  - "TheNeonHeater-9.jpg"
  - "TheNeonHeater-10.jpg"
  - "TheNeonHeater-11.jpg"
  - "TheNeonHeater-12.jpg"
  - "TheNeonHeater-13.jpg"
  
video: "https://iframe.mediadelivery.net/embed/21069/c1759cf6-8c17-4d3e-ae9c-abe7519c7d26?autoplay=true"
---

