---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: The Transmutation Of Flesh To Gold
slug: thetransmutationoffleshtogold
warnickNumber: 18.02::SCPT:675

tags:
  - work
  - w2s

type: "sculpture"
year: "2018"
featured_image: "_MG_9839.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Offset Prints Produced by those incaerated at Pickaway Correctional Institution"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "A stack of $100 dollar bills equal to the $162,510,000, the profits for Correction Corporation of America for 2017"
  long: "A stack of $100 dollar bills equal to the $162,510,000, the profits for Correction Corporation of America for 2017"


dimensions_sort: "110592"
dimensions: 'Dimensions Variable'

available: true
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/thetransmutationoffleshtogold/_MG_9841.jpg"
  - "./src/work/thetransmutationoffleshtogold/_MG_9837.jpg"
  - "./src/work/thetransmutationoffleshtogold/money-1-2.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
