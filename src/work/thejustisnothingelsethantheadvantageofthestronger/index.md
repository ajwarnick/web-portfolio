---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "The Just Is Nothing Else Than The Advantage Of The Stronger"
slug: thejustisnothingelsethantheadvantageofthestronger
warnickNumber: "19.10::SCPT:54"

tags:
  - work

type: "sculpture"
year: "2019"
featured_image: "thejustisnothingelsethantheadvantageofthestronger_1.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Gold"
  - "Marble Brick from the Acropolis of Athens"
  - "Surplus Military Blankets"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""


dimensions_sort: "1728"
dimensions: 'Dimensions Variable'
# 9" x 16" x 12"

available: true
price: "NFS"

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/thejustisnothingelsethantheadvantageofthestronger/thejustisnothingelsethantheadvantageofthestronger_2.jpg"

video:
  - {
    thumb: "https://vz-6d76e30c-3d3.b-cdn.net/9897d27c-1d83-4ac5-87e6-4948fbd840e7/preview.webp",
    video: "https://iframe.mediadelivery.net/embed/21069/9897d27c-1d83-4ac5-87e6-4948fbd840e7"
    }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
