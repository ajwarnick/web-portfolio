---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Barreira União Fabril"
slug: barrier
warnickNumber: "22.11::SCPT:44"

tags:
  - work
  - pada
  - "wassaic july 2023"

type: "sculpture"
year: "2022"
featured_image: "IMG_4464_thumb.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Plastic White Carnation"
  - "Newsprint"
  - "Plastic Chain"
  - "Companhia União Fabril Barrier"

description:
  short: ""
  long: ""


dimensions_sort: "136080"
dimensions: '108" x 42" x 30"'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/barrier/IMG_4400.jpg"
  - "./src/work/barrier/IMG_4464.jpg"
  - "./src/work/barrier/IMG_4563.jpg"
  - "./src/work/barrier/IMG_4567.jpg"
  - "./src/work/barrier/IMG_4573.jpg"
  - "./src/work/barrier/IMG_4577.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
