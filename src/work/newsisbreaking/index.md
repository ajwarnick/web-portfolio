---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: News Is Breaking
slug: newsisbreaking
warnickNumber: "21.07::SCPT:65"
tags:
  - work

type: sculpture
year: "2021"
featured_image: "News_Is_Breaking-3.jpeg"
featured_audio: ""
featured_video: ""

materials: 
  - "Clear acrylic"
  - "Galvanized Steel"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

color: '#FF0000'
description:
  short: Public sculpture currently installed at Wassaic Project Wassaic, NY. 
  long: Public sculpture made of clear acrylic currently installed at Wassaic Project Wassaic, NY. I think about the news as a lens through which we view the world, and the world as a backdrop to the events we live through. In the studio, I play with words as often as objects, and this billboard plays with how the attention economy has broken our consideration of information.


dimensions_sort: "18000"
dimensions: '120” x 50” x 3”'

available: true
price: "$4,000"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

links:
  - [
      "Public Art in Wassaic",
      "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
    ]

support_images: 
  - "./src/work/newsisbreaking/News_Is_Breaking-1.jpeg"
  - "./src/work/newsisbreaking/News_Is_Breaking-2.jpeg"
#   - "./src/work/newsisbreaking/News_Is_Breaking-4.jpeg"
#   - "./src/work/newsisbreaking/News_Is_Breaking-5.jpeg"
#   - "./src/work/newsisbreaking/News_Is_Breaking-6.jpeg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

location: "Wassaic, NY"

collection:
 - "Walker Art Center"

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
