---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "An Artist Without Debt Is No Artist (After Mladen Stilinović)"
slug: anartistwithoutdebtisnoartist
warnickNumber: 15.02::TEX:45

tags:
  - work

type: "textile"
year: "2015"
featured_image: "IMG_4578.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Cotton Cloth"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "Banner produced in the same manner as Stilinović’s An Artist Who Cannot Speak English Is No Artist to draw attention to both the historic creative indebtedness and financial position of contemporary artist."
  long: "Banner produced in the same manner as Stilinović’s An Artist Who Cannot Speak English Is No Artist to draw attention to both the historic creative indebtedness and financial position of contemporary artist."

dimensions_sort: "4320"
dimensions: '72” x 60”'

available: true
price: "NFS"

visibility: true

support_images: 
  - "./src/work/anartistwithoutdebtisnoartist/IMG_4548.jpg"
  - "./src/work/anartistwithoutdebtisnoartist/IMG_4545.jpg"
  - "./src/work/anartistwithoutdebtisnoartist/IMG_4525.jpg"
  - "./src/work/anartistwithoutdebtisnoartist/IMG_4521.jpg"
  - "./src/work/anartistwithoutdebtisnoartist/IMG_4518.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
