---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Mad Libs
slug: madlibs
warnickNumber: "21.14::PRT:58:E.ULTD"

tags:
  - work
  - msu

type: "print"
year: "2021"
featured_image: "madlib_2.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Custom Software"
  - "Toner"
  - "Bond Paper"

edition: 
#   number: 
#   proofs: 
  limitless: true

# color: ''

description:
  short: "Unique limitless takeaways germinated by a piece of custom software written to scrape the New York Times for headlines and convert them to Madlibs"
  long: "Unique limitless takeaways germinated by a piece of custom software written to scrape the New York Times for headlines and convert them to Madlibs"


dimensions_sort: "187"
dimensions: '11” x 17”'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

visibility: true

links:
  - [
      "Live generated Mad Libs",
      "http://projectspace.anthonywarnick.com/madlibs/",
    ]

support_images: 
  - "./src/work/madlibs/madlib_1.png"
#   - "./src/work/madlibs/madlib_3.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
