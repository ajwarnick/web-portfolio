---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Tower of Power"
slug: towerofpower
warnickNumber: "22.05::SCPT:49"

tags:
  - work
  - pada

type: "sculpture"
year: "2022"
featured_image: "IMG_4500.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Stainless Steel Bird Spikes"
  - "Plastic Fruit Crates"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""


dimensions_sort: "186624"
dimensions: 'Dimensions Variable'

available: false
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/towerofpower/IMG_4491.jpg"
  - "./src/work/towerofpower/IMG_4505.jpg"
  - "./src/work/towerofpower/IMG_4508.jpg"


# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

location: "Distroyed"

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
