---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: One Hundred And Fifty More
slug: onehundredandfiftymore
warnickNumber: 17.09::DGT:80

tags:
  - work
  - augsburg

type: "website"
year: "2017"
featured_image: "screenshot2022-10-22at6.06.44pm.png"
featured_audio: ""
featured_video: ""

materials: 
  - "Custom Software"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "The film, The Defiant Ones (1968) slowed down so that it will take 150 years to play through. This is packaged as a custom application that plays only where we are in the film."
  long: "The film, The Defiant Ones (1968) slowed down so that it will take 150 years to play through. This is packaged as a custom application that plays only where we are in the film."


dimensions_sort: "1500000"
dimensions: '150 Years'

available: true
price: "NSF"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

links:
  - [
      "Online Version | Project Space",
      "https://projectspace.anthonywarnick.com/One_Hundred_And_Fifty_More/",
    ]

# support_images: 
#   - "./src/work/criamosoquenaotemos/criamos_2.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
