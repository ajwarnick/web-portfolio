---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Letter Station
slug: letterstation
warnickNumber: "21.13::SND:39"

tags:
  - work
  - msu

type: "sound"
year: "2021"
featured_image: "letterstation.jpg"
featured_audio: ""
featured_video: "https://iframe.mediadelivery.net/embed/21069/03124172-414d-458c-ae2a-10660923326d"

materials: 
  - "Radio Broadcast"
  - "Custom Software"
  - "New York Times Headlines"

edition: 
  number: 4
  proofs: 1
#   limitless: true

# color: ''

description:
  short: "This work broadcasts a voice reading the current New York Times headlines in NATO code."
  long: "This work broadcasts a voice reading the current New York Times headlines in NATO code."


dimensions_sort: "10000"
dimensions: 'Live Feed'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# support_images: 
#   - "./src/work/criamosoquenaotemos/criamos_2.jpg"
#   - "./src/work/criamosoquenaotemos/criamos_3.jpg"
#   - "./src/work/criamosoquenaotemos/criamos_4.jpg"
#   - "./src/work/criamosoquenaotemos/takeaway.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
