---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Portrait of the Directors of MTC Management and Training Corporation (After Francis Galton)
slug: managementandtrainingcorporation
warnickNumber: 17.04::PH:80

tags:
  - work
  - spaces

type: "photography"
year: "2017"
featured_image: "IMG_0236.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Archival pigment print"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "The group portraits of the directors of the three largest corporations that own and administer private prisons in the United States were produced through a process of photographic layering developed by Francis Galton."
  long: "The group portraits of the directors of the three largest corporations that own and administer private prisons in the United States were produced through a process of photographic layering developed by Francis Galton. With this process Galton (an early figure in Eugenics) believed that the typical face could be captured through averaging the images of Jewish men or criminals, for example. He believed that groups were guilty based on race. Here the process has been flipped and those depicted are those profiting from the current system."

dimensions_sort: "540"
dimensions: '20" x 27"'

available: true
price: "$1,200"

visibility: true

support_images: 
  - "./src/work/managementandtrainingcorporation/IMG_0239.jpg"
  - "./src/work/managementandtrainingcorporation/Group3_enlarged_crop.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---

