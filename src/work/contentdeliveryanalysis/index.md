---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Content Delivery Analysis
slug: contentdeliveryanalysis
warnickNumber: 23.05::INST:25

tags:
  - work

type: "installation"
year: "2023"
featured_image: "_MG_8577-Enhanced-NR.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Dyed Rice Flour"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""

dimensions_sort: "3024"
dimensions: '42" x 72"'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/contentdeliveryanalysis/_MG_8588-Enhanced-NR-Edit.jpg"
  - "./src/work/contentdeliveryanalysis/_MG_8724-Enhanced-NR.jpg"
  - "./src/work/contentdeliveryanalysis/_MG_8737-Enhanced-NR.jpg"







# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]



exhibitions:
   - {
     title: "And So It Goes",
     link: "https://www.msubillings.edu/gallery/schedule%20f23-sp24.htm",
     dates: "October 12 - November 9, 2023",
     venue: "MSU, Billings"
   }


  
# eleventyNavigation:
#   key: News is Breaking
---
