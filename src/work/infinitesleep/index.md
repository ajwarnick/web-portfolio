---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Infinite Sleep
slug: infinitesleep
warnickNumber: "18.03::SCPT:03"

tags:
  - work
  - w2s

type: "sculpture"
year: "2018"
featured_image: "_MG_9825.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Prisoner Produced Pillows"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "A pillar created from 32 textured vinyl covered pillows produced by and for those incarcerated."
  long: "'Infinite Sleep' consists of an eleven foot pillar mimicking Brâncuși's Endless Column created from 32 textured vinyl pillows. Those incarcerated at Allen-Oakwood Correctional Institution in Lima, Ohio manufactured these pillows for use by other prisoners in the state of Ohio. This work is a continuation of my engagement with charged materials generally and the prison industrial complex specifically, and exemplifies my use of physical presence to engage the viewer both conceptually and phenomenologically."

dimensions_sort: "49368"
dimensions: '132" x 17" x 22"'

available: true
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/infinitesleep/_MG_9833.jpg"
  - "./src/work/infinitesleep/_MG_9831.jpg"
  - "./src/work/infinitesleep/_MG_9827.jpg"
  - "./src/work/infinitesleep/_MG_9825-2.jpg"


# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
