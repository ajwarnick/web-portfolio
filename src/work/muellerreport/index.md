---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Mueller Report (After Jenny Holzer) (For Kazimir Malevich)
slug: muellerreport
warnickNumber: "19.11::PRT:89:E.ULTD"

tags:
  - work

type: "print"
year: "2019"
featured_image: "muellerreport-1.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Bond Paper"
  - "Toner"

edition: 
#   number: 
#   proofs: 
  limitless: true

# color: ''

description:
  short: ""
  long: ""


dimensions_sort: "187"
dimensions: '11” x 17”'

available: true
price: "Free"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# support_images: 
#   - "./src/work/criamosoquenaotemos/criamos_2.jpg"
#   - "./src/work/criamosoquenaotemos/criamos_3.jpg"
#   - "./src/work/criamosoquenaotemos/criamos_4.jpg"
#   - "./src/work/criamosoquenaotemos/takeaway.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
