---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: One-Room Schoolhouse
slug: oneroomschoolhouse
warnickNumber: 13.06::INST:96

tags:
  - work

type: "installation"
year: "2013"
featured_image: "main.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "2x4"
  - "OSB"
  - "Pedagogy"
  - "Chalkboards"
  - "Students"
  - "Cast Iron Wood Burning Stove"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "Building on the traditions of the rural one-room schoolhouses, a one-room schoolhouse was built on a frozen lake to model an all-inclusive, non-hierarchical learning process."
  long: "Building on the traditions of the rural one-room schoolhouses, a one-room schoolhouse was built on a frozen lake to model an all-inclusive, non-hierarchical learning process."

dimensions_sort: "5308416"
dimensions: '192" x 192" x 144"'

available: true
price: "NFS"

visibility: true

support_images: 
  - "./src/work/oneroomschoolhouse/dsci0304.jpg"
  - "./src/work/oneroomschoolhouse/fireplaceweb.jpg"
  - "./src/work/oneroomschoolhouse/hbweb.jpg"
  - "./src/work/oneroomschoolhouse/opening-day-1.jpg"
  - "./src/work/oneroomschoolhouse/schoolhouse-interior.jpg"
  - "./src/work/oneroomschoolhouse/shelvesweb.jpg"
  - "./src/work/oneroomschoolhouse/132796435302.jpg"
  - "./src/work/oneroomschoolhouse/132796441512.jpg"
  - "./src/work/oneroomschoolhouse/132796443815.jpg"
  - "./src/work/oneroomschoolhouse/shelvesweb.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
