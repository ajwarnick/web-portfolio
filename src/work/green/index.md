---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Untitled (Gereen)"
slug: green
warnickNumber: "22.11::SCPT:21"

tags:
  - work
  - pada
  - "wassaic july 2023"

type: "sculpture"
year: "2022"
featured_image: "IMG_4440.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Cardboard"
  - "Acrylic Paint"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: "Found cardboard from the industrial park surrounding PADA manufactured to look like aluminum with the underside painted the green from the Portuguese flag."

# 6 feet x 8 feet x 14 inches
dimensions_sort: "62208"
dimensions: 'Dimensions Variable'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/green/IMG_4466.jpg"
  - "./src/work/green/IMG_4441.jpg"  
  - "./src/work/green/IMG_4440.jpg"
  - "./src/work/green/IMG_4432.jpg"


# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
