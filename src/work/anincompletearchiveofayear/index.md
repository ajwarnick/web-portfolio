---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "An Incomplete Archive Of A Year"
slug: anincompletearchiveofayear
warnickNumber: "20.01::PH:73"

tags:
  - work
  - msu

type: "photography"
year: "2020"
featured_image: "2020_01_12_img_4.jpg.print.5x7.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Chromogenic Prints"
  - "Custom Software"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "An archive of programmatically altered images sourced daily from the images on the front page of the New York Times throughout the whole of 2020."
  long: "An archive of programmatically altered images sourced daily from the images on the front page of the New York Times throughout the whole of 2020."


dimensions_sort: "20000"
dimensions: 'Dimensions Variable'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/anincompletearchiveofayear/test.jpg"
  - "./src/work/anincompletearchiveofayear/_MG_8668-Enhanced-NR.jpg"
  - "./src/work/anincompletearchiveofayear/_MG_8670-Enhanced-NR.jpg"
  - "./src/work/anincompletearchiveofayear/_MG_8671-Enhanced-NR.jpg"
  - "./src/work/anincompletearchiveofayear/_MG_8677-Enhanced-NR-Edit.jpg"
  - "./src/work/anincompletearchiveofayear/_MG_8687-Enhanced-NR-Edit-Cropped.jpg"
  - "./src/work/anincompletearchiveofayear/2020_02_15_img_0.jpg.print.5x10.jpg"
  - "./src/work/anincompletearchiveofayear/2020_02_14_img_1.jpg.print.4x6.jpg"
  - "./src/work/anincompletearchiveofayear/2020_01_30_img_4.jpg.print.5x10.jpg"
  - "./src/work/anincompletearchiveofayear/2020_01_18_img_1.jpg.print.5x7.jpg"
  - "./src/work/anincompletearchiveofayear/2020_01_12_img_4.jpg.print.5x7.jpg"
 
 

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
