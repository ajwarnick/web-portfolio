---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "withinthiszonestatesofexceptionloseallpower"
slug: withinthiszonestatesofexceptionloseallpower
warnickNumber: 19.11::DWG:32

tags:
  - work
  - msu
  - augsburg

type: "drawing"
year: "2018"
featured_image: "_DSC7859.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Powdered Graphite"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "A powdered graphite drawing on the floor of the gallery that was slowly destroyed by the movement of viewers."
  long: "A powdered graphite drawing on the floor of the gallery that was slowly destroyed by the movement of viewers."

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

dimensions_sort: "6912"
dimensions: '96” x 72”'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/withinthiszonestatesofexceptionloseallpower/_DSC7759.jpg"
  - "./src/work/withinthiszonestatesofexceptionloseallpower/_DSC7846.jpg"
  - "./src/work/withinthiszonestatesofexceptionloseallpower/_DSC8040.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
