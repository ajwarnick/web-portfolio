---
layout: layouts/work.njk
title: "Dot Dot Dot"
slug: dotdotdot
warnickNumber: "21.12::VID:64"
tags:
  - work
  - msu

type: "video"
year: "2021"
featured_image: "dotdotdot.jpeg"
featured_audio: ''
featured_video: https://iframe.mediadelivery.net/embed/21069/c1759cf6-8c17-4d3e-ae9c-abe7519c7d26?autoplay=true

materials:
 - "Video (Color, Sound)"


color: "#FF0000"
description:
  short: '21 second excerpt (HD Video), full length 58 minutes 24 seconds three channel presentation of Fox News Primetime from August 28, 2021.'
  long: '21 second excerpt (HD Video), full length 58 minutes 24 seconds three channel presentation of Fox News Primetime from August 28, 2021.'

dimensions_sort: "2000"
dimensions: '00:58:24'

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

available: true
# price: ''
visibility: false
# links: []
# support_images: []
# video: []
# location: ''
# collection: []

---
