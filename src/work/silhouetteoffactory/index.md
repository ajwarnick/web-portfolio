---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Silhouette Of Factory"
slug: silhouetteoffactory
warnickNumber: 17.10::PRT:27

tags:
  - work
  - spaces

type: "print"
year: "2017"
featured_image: "IMG_8610.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Offset Prints Produced by those incaerated at Pickaway Correctional Institution"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "This series of limitless take-away off-set prints was produced by those incarcerated at the Pickaway Correctional Institution in Orient, Ohio. The prints retain the material charge and weight of their history, and as each viewer takes a print they wrestle with the issues of our contemporary system. The prints ultimately implicate all of us in the prison industrial complex. And once we have this knowledge we must choose how one should act. "
  long: "This series of limitless take-away off-set prints was produced by those incarcerated at the Pickaway Correctional Institution in Orient, Ohio. The prints retain the material charge and weight of their history, and as each viewer takes a print they wrestle with the issues of our contemporary system. The prints ultimately implicate all of us in the prison industrial complex. And once we have this knowledge we must choose how one should act. "

dimensions_sort: "432"
dimensions: '18" x 24"'

available: true
price: "NSF"

visibility: true

support_images: 
  - "./src/work/silhouetteoffactory/IMG_8608.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
