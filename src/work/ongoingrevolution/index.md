---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Ongoing Revolution
slug: ongoingrevolution
warnickNumber: "22.08::SCPT:32"

tags:
  - work
  - pada
  - "wassaic july 2023"

type: "sculpture"
year: "2022"
featured_image: "Ongoing_Revolution_2.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Grelha de Sardinha"
  - "Bandeira Verde-Rubra"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: "The BBQ sardine holder was ubiquitous in my time in Portugal with the Feast of St. Anthony (Festival of Sardines) taking place in the greater Lisbon metro area while I was in residence. In this work a modernist monochrome is created by strategically folding a Portuguese flag which was imported from China."

dimensions_sort: "5826"
dimensions: '14.57" x 8" x 8"'

available: true
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/ongoingrevolution/Ongoing_Revolution_1.jpg"
  - "./src/work/ongoingrevolution/Ongoing_Revolution_3.jpg"
  - "./src/work/ongoingrevolution/Ongoing_Revolution_4.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
