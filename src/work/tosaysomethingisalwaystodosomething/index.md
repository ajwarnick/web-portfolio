---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "To Say Something Is Always To Do Something"
slug: tosaysomethingisalwaystodosomething
warnickNumber: 18.05::DGT:38

tags:
  - work
  - w2s

type: "generative"
year: "2018"
featured_image: "MVI_9881-0001.png"
featured_audio: ""
featured_video: "https://iframe.mediadelivery.net/embed/21069/32ecfcf0-98f8-4b5a-bb8e-c8e83960bbcd?"

materials: 
  - "Custom Software"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""


dimensions_sort: ""
dimensions: 'Dimensions Variable'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/tosaysomethingisalwaystodosomething/_MG_9879.jpg"

video:
  - {
    video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
    thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
  }
  - {
    video: "https://iframe.mediadelivery.net/embed/21069/32ecfcf0-98f8-4b5a-bb8e-c8e83960bbcd",
    thumb: "https://vz-6d76e30c-3d3.b-cdn.net/32ecfcf0-98f8-4b5a-bb8e-c8e83960bbcd/thumbnail.jpg",
  }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
