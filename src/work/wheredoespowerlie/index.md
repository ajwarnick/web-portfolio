---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Where Does Power Lie?"
slug: wheredoespowerlie
warnickNumber: "19.02::PRT:50"

tags:
  - work

type: "Photography"
year: "2019"
featured_image: "wheredoespowerlie_5.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Vintage Postcard"
  - "Custom Stamp"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""


dimensions_sort: "15"
dimensions: '3" x 5"'

available: true
price: "$300"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/wheredoespowerlie/wheredoespowerlie_1.jpg"
  - "./src/work/wheredoespowerlie/wheredoespowerlie_2.jpg"
  - "./src/work/wheredoespowerlie/wheredoespowerlie_3.jpg"
  - "./src/work/wheredoespowerlie/wheredoespowerlie_4.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
