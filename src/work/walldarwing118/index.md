---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Wall Darwing #118 (After Sol LeWitt)"
slug: walldarwing118
warnickNumber: 16.04::DWG:81

tags:
  - work

type: "drawing"
year: "2016"
featured_image: "IMG_7529.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Labor"
  - "Veteran"
  - "Graphite"
  - "Gold"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "This work is a Sol LeWitt wall drawing drawn by a unemployed veteran who was paid for his time in gold."
  long: "This work is a Sol LeWitt wall drawing drawn by a unemployed veteran who was paid for his time in gold."

dimensions_sort: "8640"
dimensions: '72" x 120"'

available: false
price: "NFS"

visibility: true

support_images: 
  - "./src/work/walldarwing118/IMG_7618.jpg"
  - "./src/work/walldarwing118/IMG_7624.jpg"
  - "./src/work/walldarwing118/walldrawinginstructions.jpg"
  - "./src/work/walldarwing118/img_7710_edit.tif"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
