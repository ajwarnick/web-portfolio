---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Secrets Obscured Through Language #3"
slug: secretsobscuredthroughlanguage
warnickNumber: "19.15::DWG:03"

tags:
  - work

type: "drawing"
year: "2019"
featured_image: "secretsobscuredthroughlanguage_1.jpg"

materials: 
  - "Engineering Paper"
  - "Marker"

# color: ''

description:
  short: "Linear B tablet transcribed while thinking of things I would never say."
  long: "Linear B tablet transcribed while thinking of things I would never say."


dimensions_sort: "93.5"
dimensions: '8.5" x 11"'

available: true
price: "$1,200"

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/secretsobscuredthroughlanguage/secretsobscuredthroughlanguage_3.jpg"
  - "./src/work/secretsobscuredthroughlanguage/secretsobscuredthroughlanguage_2.jpg"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Habits of Thought, Patterns of Action",
#     link: "https://www.instagram.com/p/Bz_XVu0pxG3/?igshid=tpamvf0yxxjl",
#     dates: "July 19, 2019 - August 19, 2019",
#     venue: "Plug Projects"
#   }

  
# eleventyNavigation:
#   key: News is Breaking
---
