---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Newscape
slug: newscape
warnickNumber: "22.02::DGT:65"

tags:
  - work
  - msu

type: "website"
year: "2022"
featured_image: "Newscape-1.png"
featured_audio: ""
featured_video: ""

materials: 
  - "Custom Software"
  - "New York Times Images"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""


dimensions_sort: "2000"
dimensions: 'Dimensions Variable '

available: true
price: ""

support_images: 
  - "./src/work/newscape/Newscape-3.png"
  - "./src/work/newscape/Newscape-2.png"
  - "./src/work/newscape/Newscape-4.png"
  - "./src/work/newscape/_MG_9036.jpg"
  - "./src/work/newscape/_MG_9134-Enhanced-NR.jpg"
  - "./src/work/newscape/_MG_9144.jpg"
  - "./src/work/newscape/_MG_9145-Enhanced-NR.jpg"
  - "./src/work/newscape/_MG_9148-Enhanced-NR.jpg"
  

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

visibility: true

links:
  - [
      "Online Version | Project Space",
      "https://projectspace.anthonywarnick.com/newscape/",
    ]



# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
