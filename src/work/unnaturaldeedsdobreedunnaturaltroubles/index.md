---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Unnatural Deeds Do Breed Unnatural Troubles
slug: unnaturaldeedsdobreedunnaturaltroubles
warnickNumber: "19.09::SCPT:91"

tags:
  - work

type: "sculpture"
year: "2019"
featured_image: "unnaturaldeedsdobreedunnaturaltroubles_2.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Gold"
  - "Prisoner Produced Pillow"

# color: ''

description:
  short: "A gilded textured vinyl covered pillow produced by and for those incarcerated. The pillow was manufactured by those incarcerated at Allen-Oakwood Correctional Institution in Lima, Ohio."
  long: "A gilded textured vinyl covered pillow produced by and for those incarcerated. The pillow was manufactured by those incarcerated at Allen-Oakwood Correctional Institution in Lima, Ohio."


dimensions_sort: "1620"
dimensions: '18" x 15" x 6"'

available: true
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/unnaturaldeedsdobreedunnaturaltroubles/unnaturaldeedsdobreedunnaturaltroubles_1.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
