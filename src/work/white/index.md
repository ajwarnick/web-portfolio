---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Untitled (White)"
slug: white
warnickNumber: 17.07::TEX:27

tags:
  - work
  - spaces

type: "textile"
year: "2017"
featured_image: "IMG_0279.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Disassembled American flags produced by those incarcerated in the state of Ohio"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "This fabric wall hanging was modeled on the Christmas paper chains children produce to celebrate the season. This chain was produced from the white stripes of disassembled American flags. These flags were produced by those incarceratedin the state of Ohio."
  long: "This fabric wall hanging was modeled on the Christmas paper chains children produce to celebrate the season. This chain was produced from the white stripes of disassembled American flags. These flags were produced by those incarcerated in the state of Ohio."

dimensions_sort: "4000"
dimensions: 'Dimensions Variable'

available: true
price: "NFS"

visibility: true

support_images: 
  - "./src/work/white/IMG_0303.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
