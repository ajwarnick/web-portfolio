---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "The Books That Have Not Been Checked Out Between September 6, 2013, and April 18, 2015"
slug: books
warnickNumber: 15.08::INST:62

tags:
  - work

type: "installation"
year: "2015"
featured_image: "IMG_5063.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "3,000 Books"
  - "Offset Print"
  - "Labor"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: 'A performance that created an object consisting of, 3,000 books that had not been checked out from the library at the Cranbrook Academy of Art during my time there. These books where manually found, checked out, stamped, and then hand carried to the final installation, resulting in a 75" x 94" x 58” monument to forgotten ideas.'
  long: 'A performance that created an object consisting of, 3,000 books that had not been checked out from the library at the Cranbrook Academy of Art during my time there. These books where manually found, checked out, stamped, and then hand carried to the final installation, resulting in a 75" x 94" x 58” monument to forgotten ideas.'

dimensions_sort: "408900"
dimensions: '75" x 94" x 58”'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/books/IMG_5948.jpg"
  - "./src/work/books/IMG_5445.jpg"
  - "./src/work/books/IMG_5081.jpg"
  - "./src/work/books/BookStamp.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
