---
layout: layouts/work.njk
permalink: work/abstractnewspaper/index.html
title: "Abstract Newspaper"
slug: abstractnewspaper
warnickNumber: "21.16::SCPT:65"
tags:
  - work
  - msu

type: "sculpture"
year: "2021"
featured_image: "Warnick_Anthony_Abstract_News_Paper_2.jpg" 
# featured_audio: ""
# featured_video: ""

materials: 
  - "Newsprint"
  - "Ink"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

color: '#FF0000'
description:
  short: A daily updated stack of ten New York Times that has been programmatically altered rendering the information to pure form.  
  long: A daily updated stack of ten New York Times that has been programmatically altered rendering the information to pure form. 


dimensions_sort: "858"
dimensions: '11” x 13” x 6”'

available: true
price: "$2,000"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/abstractnewspaper/Warnick_Anthony_Abstract_News_Paper_1.jpg"
  - "./src/work/abstractnewspaper/Warnick_Anthony_Abstract_News_Paper_3.jpg"


# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
