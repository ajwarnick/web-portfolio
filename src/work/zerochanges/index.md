---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Violent Policing. Livid Protesters. Zero Changes."
slug: zerochanges
warnickNumber: 23.04::DWG:57

tags:
  - work

type: "drawing"
year: "2023"
featured_image: "_MG_8713-Enhanced-NR.jpg"
featured_audio: ""
featured_video: ""

materials:
  - "Charcoal"
  - "Gypsum on Poplar Panel"

description:
  short: ""
  long: ""

dimensions_sort: "384"
dimensions: '48" x 8"'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/zerochanges/_MG_8715-Enhanced-NR-Edit.jpg"
  - "./src/work/zerochanges/_MG_8700-Enhanced-NR.jpg"


exhibitions:
   - {
     title: "And So It Goes",
     link: "https://www.msubillings.edu/gallery/schedule%20f23-sp24.htm",
     dates: "October 12 - November 9, 2023",
     venue: "MSU, Billings"
   }

---
