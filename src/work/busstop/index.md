---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Bus Stop for Bloomfield Hills"
slug: busstop
warnickNumber: 14.18::SCPT:77 

tags:
  - work
  - cranbrook

type: "sculpture"
year: "2014"
featured_image: "1Q9A2726.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Steel Sign"
  - "Bolts"
  - "Galvanized Steel"
  - "Dirt"
  - "Bus Schedule"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "The installation of a new bus stop in a community that has banned buses to keep out undesirables. The bus stop was then used to catch the bus. After one week the city removed the bus stop."
  long: "The installation of a new bus stop in a community that has banned buses to keep out undesirables. The bus stop was then used to catch the bus. After one week the city removed the bus stop."

dimensions_sort: "1980"
dimensions: '5" x 3" x 132"'

available: true
price: "NFS"

visibility: true

support_images: 
  - "./src/work/busstop/1Q9A2729.jpg"
  - "./src/work/busstop/1Q9A2743.jpg"
  - "./src/work/busstop/1Q9A2762.jpg"
  - "./src/work/busstop/DSC_7766.jpg"
  - "./src/work/busstop/bus install-1.jpg"
  - "./src/work/busstop/bus install-4.jpg"
  - "./src/work/busstop/bus install-2.jpg"
  - "./src/work/busstop/bus install-3.jpg"
  

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
