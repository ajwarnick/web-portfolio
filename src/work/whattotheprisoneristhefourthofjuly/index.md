---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "What To The Prisoner Is The Fourth Of July?"
slug: whattotheprisoneristhefourthofjuly
warnickNumber: 17.03::TEX:15

tags:
  - work
  - spaces

type: "textile"
year: "2017"
featured_image: "IMG_0283.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Disassembled American flags produced by those incarcerated in the state of Ohio"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "The text embroidered into the flag is adapted from Frederick Douglass, and asks “What to the prisoner is the Fourth of July?” The tapestry is produced from the red stripes of disassembled American flags. These flags were produced by prison labor in the state of Ohio."
  long: "The text embroidered into the flag is adapted from Frederick Douglass, and asks “What to the prisoner is the Fourth of July?” The tapestry is produced from the red stripes of disassembled American flags. These flags were produced by prison labor in the state of Ohio."

dimensions_sort: "6912"
dimensions: '72" x 96"'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/whattotheprisoneristhefourthofjuly/IMG_0247.jpg"
  - "./src/work/whattotheprisoneristhefourthofjuly/IMG_0256.jpg"
  - "./src/work/whattotheprisoneristhefourthofjuly/IMG_0297.jpg"
  - "./src/work/whattotheprisoneristhefourthofjuly/IMG_0301.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
