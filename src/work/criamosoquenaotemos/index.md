---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Criamos O Que Não Temos"
slug: criamosoquenaotemos
warnickNumber: " 22.10::PRT:57:E.ULTD"
tags:
  - work
  - pada
  - "wassaic july 2023"

type: print
year: "2022"
featured_image: "criamos_1.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Toner"
  - "Bond Paper"

edition: 
  number: 300
  proofs: 0
  limitless: true

# color: ''

description:
  short: ""
  long: "Free A3 takeaway with ID portraits of the workers from the old Companhia União Fabril (CUF) industrial park. These images where sourced from the CUF museum. The phrase surrounding the workers is a play on the company's motto, created by founder Alfredo da Silva \"What the country doesn’t have CUF creates.\" The new motto translates to \"what we don't have we create\" or \"we create what we don't have\" depending on where one starts reading."


dimensions_sort: "1260"
dimensions: '30cm x 42cm'

available: true
price: "NSF"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/criamosoquenaotemos/criamos_2.jpg"
  - "./src/work/criamosoquenaotemos/criamos_3.jpg"
  - "./src/work/criamosoquenaotemos/criamos_4.jpg"
  - "./src/work/criamosoquenaotemos/takeaway.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
