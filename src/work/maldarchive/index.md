---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Mal d'Archive
slug: maldarchive
warnickNumber: 18.07::PERF:14

tags:
  - work

type: "performance"
year: "2018"
featured_image: "IMG_9157.jpg"
featured_audio: ""
featured_video: "https://iframe.mediadelivery.net/embed/21069/ecbd45ba-81b1-44c6-82e7-98b7eb9e6a18"

materials: 
  - "Court Records"
  - "Fire"

edition: 
  number: 3
  proofs: 1
#   limitless: true

# color: ''

description:
  short: "The perforative burning of a stolen court records"
  long: "The performances are gestures intended to express an idea through the insertion of bodies into systems. For example, the perforative burning of a stolen prisoner records as in Mal d’Archive. Resulting in a rumination on what condemns, is it the judge or the archive? The work is both object and performance."


dimensions_sort: "300"
dimensions: '00:00:44'

available: true
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/maldarchive/IMG_9157.jpg"
  - "./src/work/maldarchive/IMG_9160.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
