---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Gideon's Trumpet"
slug: gideonstrumpet
warnickNumber: "18.06:PRT:43"

tags:
  - work
  - w2s

type: "print"
year: "2018"
featured_image: "_MG_9783-Edit_crop.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Bond Paper"
  - "Toner"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "24 frames removed from the film Gideon's Trumpet covering the second during which Clarence Earl Gideon is sentneced to Gideon five years in the state prison. This moment of <i>performative language</i> changed his life simply because he was poor. Clarence Earl Gideon's case eveturally made it to the Supreme Court and was pivitol in establishing a right to representation regardless of wealth."
  long: "24 frames removed from the film Gideon's Trumpet covering the second during which Clarence Earl Gideon is sentneced to Gideon five years in the state prison. This moment of <i>performative language</i> changed his life simply because he was poor. Clarence Earl Gideon's case eveturally made it to the Supreme Court and was pivitol in establishing a right to representation regardless of wealth."


dimensions_sort: "3696"
dimensions: '24 @ 11" x 14" (each)'

available: true
price: ""

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/gideonstrumpet/_MG_9790.jpg"
  - "./src/work/gideonstrumpet/_MG_9778.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
