---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Black And White And Red All Over (After Klara Lidén)"
slug: blackandwhiteandredallover
warnickNumber: 23.02::DWG:11

tags:
  - work

type: "drawing"
year: "2023"
featured_image: "_MG_8829.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Newsprint"
  - "Acrylic Paint"

# color: ''

description:
  short: ""
  long: ""

dimensions_sort: "776"
dimensions: '33" x 23.5"'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/blackandwhiteandredallover/_MG_8827.jpg"
  - "./src/work/blackandwhiteandredallover/_MG_8832.jpg"  
  - "./src/work/blackandwhiteandredallover/_MG_8838.jpg"





# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]
# https://www.serpentinegalleries.org/whats-on/klara-liden/


exhibitions:
   - {
     title: "And So It Goes",
     link: "https://www.msubillings.edu/gallery/schedule%20f23-sp24.htm",
     dates: "October 12 - November 9, 2023",
     venue: "MSU, Billings"
   }


  
# eleventyNavigation:
#   key: News is Breaking
---
