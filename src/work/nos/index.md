---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: Nós
slug: nos
warnickNumber: "22.09::SCPT:44"

tags:
  - work
  - pada
  - "wassaic july 2023"

type: "sculpture"
year: "2022"
featured_image: "nos_3.jpg"
# featured_audio: ""
# featured_video: ""

materials: 
  - "Tarpaulin"
  - "Cotton Canvas"
  - "Acrylic Paint"
  - "PVC"
  - "Terracotta Brick"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: "Recreation of the Companhia União Fabril (CUF) Flag found in the museum at their former industrial park. In my recreation the name of the corporation was replaced with the Portuguese collective pronoun for we."


dimensions_sort: "33241"
# dimensions: '226cm x 11cm x 213cm'
dimensions: '89" x 4½" x 83"'

available: true
price: "NSF"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/nos/nos_2.jpg"
  - "./src/work/nos/nos_6.jpg"
  - "./src/work/nos/nos_4.jpg"
  - "./src/work/nos/nos_5.jpg"
  - "./src/work/nos/nos_1.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
