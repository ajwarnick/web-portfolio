---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "The Library"
slug: thelibrary
warnickNumber: 13.09::INST:80

tags:
  - work

type: "installation"
year: "2013"
featured_image: "_MG_4647.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Bond Paper"
  - "Words"
  - "Custom Software"
  - "Patrons"


# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "A fully functional library comprised of books downloaded from the internet which have had their language altered by custom software. Generously made possible by the Jerome Foundation through Northern Lights MN."
  long: "A fully functional library comprised of books downloaded from the internet which have had their language altered by custom software. Generously made possible by the Jerome Foundation through Northern Lights MN."

dimensions_sort: "500000"
dimensions: 'Dimensions Variable'

available: true
price: "NFS"

visibility: true

support_images: 
  - "./src/work/thelibrary/_MG_4651.jpg"
  - "./src/work/thelibrary/IMG_0210.jpg"
  - "./src/work/thelibrary/IMG_0224.jpg"
  - "./src/work/thelibrary/IMG_0228.jpg"
  - "./src/work/thelibrary/IMG_0344.jpg"
  - "./src/work/thelibrary/IMG_0354.jpg"
  - "./src/work/thelibrary/IMG_0355.jpg"
  - "./src/work/thelibrary/IMG_0392-2.jpg"
  - "./src/work/thelibrary/IMG_0360.jpg"
  - "./src/work/thelibrary/IMG_0390.jpg"
  - "./src/work/thelibrary/IMG_0437.jpg"
  - "./src/work/thelibrary/IMG_0410.jpg"
  - "./src/work/thelibrary/IMG_0407.jpg"
  - "./src/work/thelibrary/IMG_0392.jpg"
  - "./src/work/thelibrary/IMG_0493.jpg"
  - "./src/work/thelibrary/_MG_4648.jpg"
  

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
