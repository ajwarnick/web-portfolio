---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "Illegal Document: Or Attempt at Equal Exchange"
slug: illegaldocumentexchange
warnickNumber: 16.05::PH:31

tags:
  - work

type: "photography"
year: "2016"
featured_image: "illegaldocuments-front.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Archival Pigment Print"

edition: 
  number: 1
  proofs: 1
#   limitless: true

# color: ''

description:
  short: "A photograph of a document that make me ‘legal’. [my social security card is printed life size in the chromogenic color print.] I gave one of the two prints to an undocumented worker in addition to his day rate for the creation of a painting."
  long: "A photograph of a document that make me ‘legal’. [my social security card is printed life size in the chromogenic color print.] I gave one of the two prints to an undocumented worker in addition to his day rate for the creation of a painting."

dimensions_sort: "864"
dimensions: '24” x 36”'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/illegaldocumentexchange/illegaldocuments-back.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
