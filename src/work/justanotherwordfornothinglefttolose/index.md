---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "________________ Is Just Another Word For Nothing Left To Lose"
slug: justanotherwordfornothinglefttolose
warnickNumber: "14.08::SCPT:14"

tags:
  - work

type: "sculpture"
year: "2014"
featured_image: "IMG_2504.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Maple"
  - "Canvas"
  - "Acrylic Paint"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: "An existential protest based on the hit single “Me and Bobby McGee” written by Kris Kristofferson."
  long: ""

dimensions_sort: "12180"
dimensions: '87” x 70” x 2"'

available: true
price: ""

visibility: true

support_images: 
  - "./src/work/justanotherwordfornothinglefttolose/IMG_2505.jpg"
  - "./src/work/justanotherwordfornothinglefttolose/MVI_2502-2.jpg"

# video:
#   - {
#     video: "https://iframe.mediadelivery.net/embed/21069/51283ab6-8f4c-40b8-9d03-58ac4d71df9c",
#     thumb: "https://vz-6d76e30c-3d3.b-cdn.net/51283ab6-8f4c-40b8-9d03-58ac4d71df9c/thumbnail.jpg",
#   }

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

collaborators:
  - {
    name: "Henrik Munk Soerensen",
    link: "https://www.henrikmunksoerensen.com/"
  }


# location: ""

# collection:
#  - ""

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
