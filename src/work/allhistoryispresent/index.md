---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "All History Is Present"
slug: allhistoryispresent
warnickNumber: "19.21::SCPT:94"

tags:
  - work

type: "sculpture"
year: "2019"
featured_image: "allhistoryispresent_3.jpg"
featured_audio: ""
featured_video: ""

materials: 
  - "Altered Art History Slide"
  - "Overhead Projector"

# edition: 
#   number: 
#   proofs: 
#   limitless: true

# color: ''

description:
  short: ""
  long: ""

dimensions_sort: "62208"
dimensions: 'Dimensions Variable'
# About 72 x 36 x 72

available: true
price: "NFS"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

support_images: 
  - "./src/work/allhistoryispresent/allhistoryispresent_1.jpg"
  - "./src/work/allhistoryispresent/allhistoryispresent_2.jpg"
  - "./src/work/allhistoryispresent/allhistoryispresent_4.jpg"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
