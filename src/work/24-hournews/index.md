---
layout: layouts/work.njk
# permalink: work/newsisbreaking/index.html
title: "20 24-Hour News Sources"
slug: 24-hournews
warnickNumber: "21.01::VID:71"

tags:
  - work
  - msu

type: "video"
year: "2021"
featured_image: "24-hournews-1.png"
featured_audio: ""
featured_video: "https://iframe.mediadelivery.net/embed/21069/56016cb9-4fc6-4d91-8a51-652d31b76211"

materials: 
  - "Video (Color, Sound)"

edition: 
  number: 4
  proofs: 1
#   limitless: true

# color: ''

description:
  short: "This work is a stream of the following news channels: BBC News Channel, Sky News, Euronews, France 24, RT, Deutsche Welle, NHK World, CCTV News Channel, New Delhi Television (NDTV), Al Jazeera, Al Arabiya, A24, SABC News, CNN, Fox News Channel, MSNBC, Zee Media Corporation (WION), Africa 24, Walta TV, and Telesur"
  long: "This work is a stream of the following news channels: BBC News Channel, Sky News, Euronews, France 24, RT, Deutsche Welle, NHK World, CCTV News Channel, New Delhi Television (NDTV), Al Jazeera, Al Arabiya, A24, SABC News, CNN, Fox News Channel, MSNBC, Zee Media Corporation (WION), Africa 24, Walta TV, and Telesur"


dimensions_sort: "10000"
dimensions: 'Live Feed'

available: true
price: "$1,500"

# collaborators:
#   - {
#     name: "Danni O'Brien",
#     link: "http://www.danielleobrienart.com/"
#   }

visibility: true

# links:
#   - [
#       "Public Art in Wassaic",
#       "https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918",
#     ]

exhibitions:
  - {
    title: "And So It Goes",
    venue: "Northcutt Steele Gallery",
    base: "MSU",
  }

support_images: 
  - "24-hournews-1.png"
  - "./src/work/24-hournews/24-hournews-2.png"
  - "./src/work/24-hournews/24-hournews-3.png"

# video:
#   - ""

# audio:
#   - "/audio/audio.mp3"

# assets: 
#   - {
#     type: "bunny",
#     title: "Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report"
#   }
#   - {
#     type: "image",
#     title: "Image from Greace",
#     link: "/img/61_1-warnick-greece-show.jpg"
#   }

# location: ""

# collection:
#  - ""

# exhibitions:
#   - {
#     title: "Piotr Szyhalski / Labor Camp – COVID-19: Labor Camp Report",
#     link: "https://new.artsmia.org/exhibition/piotr-szyhalski-labor-camp-covid-19-labor-camp-report",
#     dates: "March 17, 2021 - September 19, 2021",
#     venue: "MIA"
#   }
#   - {
#     title: "A Bridge Divides, A Bridge Transforms",
#     link: "https://new.artsmia.org/exhibition/siah-armajani-a-bridge-divides-a-bridge-transforms",
#     dates: "March 19, 2021 - September 13, 2021",
#     venue: "Minneapolis Institute of Art"
#   }
  
# eleventyNavigation:
#   key: News is Breaking
---
