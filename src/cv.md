---
layout: layouts/base.njk
eleventyNavigation:
  key: CV
templateClass: cv
---

Bio:

Anthony Warnick investigates the intersection of social systems and capital. He lives and works in Manhattan, Kansas. Warnick holds a MFA in Sculpture from the Cranbrook Academy of Art and a BFA from Minneapolis College of Art and Design in Web + Multimedia Environments. He works between digital media and sculpture producing works in media as varied as film, porcelain, software, and paper. These works have been exhibited nationally and internationally at Carnation Contemporary (Portland, OR), Ely Center of Contemporary Art (New Haven, CT), SPACES Gallery (Cleveland, OH), Museum of Contemporary Art (Detroit, MI), Urban Institute for Contemporary Arts (Grand Rapids, MI), Concordia University (Montreal, Quebec), and CICA Museum (Gimpo-si, Gyeonggi-do, Korea). Warnick's work has been supported with residencies at SOMA (Mexico City), PADA (Barreiro, Portugal), Elsewhere (Greensboro, NC), Wassaic Project (Wassaic, NY), and Futurefarmers (San Francisco, CA). 


Anthony Warnick  
anthony@anthonywarnick.com  

## Exhibitions

### 2024
_phone, wallet, KEYS_ at Coco Hunday, Tampa, FL  
_Lines Crossing: Intersections of Art and Technology_, at The Richardson IQ, University of Texas at Dallas, Dallas, TX  

### 2023
*[\_\_\_\_\_\_\_\_\_\_ News](/exhibition/neonheater/)* (Solo) at The Neon Heater, Findlay, OH    
_[And So It Goes](/exhibition/MSU/)_ (Solo) at Northcutt Steele Gallery, Montana State University, Billings, MT   
_6x6_ at Rochester Contemporary Art Center, Rochester, NY  
_Pop Up_ at Mark A. Chapman Gallery, Kansas State Univeristy, Manhattan, KS  

### 2022
_A Changing World_, ImageOHIO (FotoFocus 2022 Biennial), Columbus, OH  
_034_, PADA, Barreiro, Portugal  
*Color* at CICA Museum, Gimpo-si, Gyeonggi-do, Korea  
[*Salina Biennial*](https://www.salinaartcenter.org/2022-salina-biennial) at Salina Art Center, Salina, KS  
[*UNDERCURRENTS*](https://elycenter.org/undercurrent) at Ely Center of Contemporary Art, New Haven, CT  

### 2021
[Public Sculpture](https://www.wassaicproject.org/exhibitions/public-art#block-yui_3_17_2_1_1635259463800_75918) at Wassaic Project, Wassaic, NY  
Group Show at Shrine Gallery, New York, NY  
[*Uncompressed*](https://www.coastal.edu/gallery/pastexhibitions/) at Rebecca Randall Bryan Art Gallery, Coastal Carolina University, Conway, SC  
_AOVx_ at Minnesota Museum of American Art, St. Paul, MN (Canceled due to COVID-19)  

### 2020
*Catalyst* (NCECA satellite) at Cherry Gallery, Richmond, VA (Canceled due to COVID-19)  

### 2019
[*On the Tip of My Tongue*](https://carnationcontemporary.com/On-the-Tip-of-My-Tongue-Katherine-Spinella-Kristin-Hough) at Carnation Contemporary, Portland, OR  
*Habits of Thought, Patterns of Action* (Two Person) at Plug Projects, Kansas City, MO  
*Breaching the Margins* at Urban Institute for Contemporary Arts, Grand Rapids, MI  
*What Makes Democracy?* (Solo) with FaveLAB, Athens, Greece  
*Vitrine* at Plexus Projects, Brooklyn, NY  

### 2018  
*NON//SENSE* at the Gordon Square Arts Space, Cleveland, OH  
*Uncommon Senses II* at Concordia University, Montreal, QC  
[*Language Games*](https://sculpturecenter.org/anthony_warnick/) (Solo) at The Sculpture Center, Cleveland, OH  

### 2017   
[*The Logic Of The Exception*](https://www.augsburg.edu/galleries/2017/10/20/anthonywarnick/) (Solo) Christensen Gallery at the Augsburg College, Minneapolis, MN  
CIA Faculty Exhibition at Reinberger Gallery, Cleveland, OH  
*SONIC REBELLION: MUSIC AS RESISTANCE* at Museum of Contemporary Art Detroit, Detroit, MI  
Elsewhere Museum, Greensboro, NC  
[*Except As A Punishment for Crime*](https://www.spacescle.org/exhibitions/2016/12/01/except-as-a-punishment-for-crime) (Solo) at SPACES Gallery, Cleveland, OH  

### 2016  
CIA Faculty Exhibition at Reinberger Gallery, Cleveland, OH  
*Disappear Under Messy Possibilities* at SOMA, Mexico City  
ROY G BIV Gallery (Solo), Columbus, OH  

### 2015  
Graduate Degree Exhibition at Cranbrook Art Museum, Bloomfield Hills, MI  
*Failure After Failure* (Solo) at Detroit Gallery, Minneapolis, MI  

### 2014  
*15th Experiencing Perspectives* at Mercedes Benz Financial Services, Farmington Hills, MI  
*Pyrocognitions* at Forum Gallery, Bloomfield Hills, MI  
*Communication: The Breakdown* at Forum Gallery, Bloomfield Hills, MI  

### 2013  
*Poetry and Prosaic* at Forum Gallery Fall 2013 Bloomfield Hills, MI  
*The God Show* at Forum Gallery Fall 2013 Bloomfield Hills, MI  
Artists on the Verge at The Soap Factory, Minneapolis, MN  

### 2012  
*Rose Colored Glass* at Katherine E. Nash Gallery, University of Minnesota, Minneapolis, MN  
*Copy Cat* at The Dressing Room, Minneapolis, MN  
Art Shanty Projects, Minneapolis, MN  

### 2011  
Commencement Exhibition at Minneapolis College of Art and Design (MCAD)  
Northern Spark by Northern Lights.mn Minneapolis and St. Paul, MN  
*DBEA* at 3333, Minneapolis, MN  
*Pop Saw The Need* at Double Art Gallery, Minneapolis, MN  
*Made @ MCAD* at MCAD, Minneapolis, MN  
*Memory* at MCAD, Minneapolis, MN  
*Free Makers School* at Gallery 148, Minneapolis, MN  

### 2010  
*Situate Lost Causes!* at Art of This, Minneapolis, MN  
*Slanguage Portmanshow* at Gallery 148, Minneapolis, MN  
*Made @ MCAD* at MCAD, Minneapolis, MN  
*Foot In The Door 4* at Minneapolis Institute of Art, Minneapolis, MN  
*Time | Space* at Bi Gallery, Minneapolis, MN  

### 2009  
*Bury The Hatchet* at Art of This, Minneapolis, MN  
*Time | Space* at MCAD, Minneapolis, MN  
*Rumble on the Southside* at Art of This, Minneapolis, MN  
Artery 24 at The Soap Factory, Minneapolis, MN  


## Residencies  
Wassaic Project, Wassaic, NY 2023  
PADA Studios, Barreiro, Portugal 2022   
Wassaic Project, Wassaic, NY 2019  
FAVELab, Athens, Greece 2019  
Gordon Square Arts District, Cleveland, OH 2018  
Elsewhere Museum, Greensboro, NC 2017  
SPACES Gallery, Cleveland, OH 2016/2017  
SOMA Summer, Mexico City, Mexico 2016  
Ten Chances, St. Paul, MN 2014  
Futurefarmers, San Francisco, CA 2011  
Art of This, Minneapolis, MN 2010  


## Awards/Honors
Faculty Enhancement Program, Kansas State University, 2020-2021  
The Hopper Prize (Shortlist), 2019  
Ohio Arts Council Individual Excellence Award, 2017  
Artist on the Verge Fellowship, Northern Lights.mn, 2012 - 2013  
 


## Invited Talks / Presentations  
*Code || Art*, The Richardson IQ, University of Texas at Dallas, March 8, 2024  
*'Artist Talk'*, Wassaic Project, Wassaic, NY, July 8, 2023  
*'Except As A Punishment For Crime'*, Ninth annual Civil Rights Teach-In, Kansas State University, Jan. 25, 2023  
*‘The Roles and Values of Studio Critiques’*, National Association of Schools of Art and Design Annual Meeting, Oct. 14, 2022  
*'Network as Object'*, Cranbrook Academy of Art, Apr. 17, 2020  
Artist Presentation, Parsons School of Design, Mar. 9, 2020  
*'Artist Talk'*, Wassaic Project, Wassaic, NY, July 12, 2019   
*'What is Democracy'*, Kansas State University Mar 21, 2019  
*‘Visiting Artist Lecture’*, Washington and Lee University, Mar 2, 2017  
*‘Appropriations’*, NYU Shanghai, China, Nov 7, 2016  
*‘Can We Talk’*, SOMA, Mexico City, Aug 25, 2016  
*‘Visiting Artist Lecture’*, Cleveland Institute of Art, Sep 16, 2015  
*‘Lecture as Sculpture’*, Cranbrook Academy of Art, Mar 4, 2015  
*‘A Cranbrook Conversation’ Art in the Contemporary World Podcast, National College of Art & Design*, Dublin, Ireland - Mar. 2014  


## Publications  
URGENCY READER 2, Queer.Archive.Work 2020  
DUMP, SOMA, 2016  
Folio 15, Cranbrook Academy of Art, 2015  
15th Experiencing Perspectives Catalogue published by Mercedes Benz, 2014  
Quodlibetica - Constellation 18 February 2012  
Quodlibetica - Constellation 16 October 2011  
Not-book Issue no. 1 Fall 2011  


## Collections 
Elsewhere Museum, Greensboro, NC   
Cranbrook Art Museum, Bloomfield Hills, MI  
SOMA Library, Mexico City, Mexico  
University of St. Thomas Library (Artist Book Collection), St. Paul, MN      
The Floating Library, Minneapolis, MN   
Gordon Square Arts District, Cleveland, OH  


## Work Experience  
Co-Director/Co-Curator, The Muted Horn Gallery, Jan. 2016 - Dec. 2018  
Designer and Developer, Cranbrook Academy of Art, Dec. 2014 - May 2015  
Developer, New Media Initiatives Walker Art Center Dec. 2011 - Aug. 2013  
Developer, Aspen Art Museum Spring 2012 - Aug. 2013  
Co-Director/Co-Curator Art Of This One Nighter Series 2010 - Aug. 2013  
Developer, Futurefarmers, Dec. 2011 - Aug. 2013  
Board of Directors, Gallery 148, 2010 - 2011  

## Education

### 2013 - 2015  
M.F.A. (Sculpture)  
Cranbrook Academy of Art  

### 2007 - 2011  
B.F.A. (Web + Multimedia Environments)  
Minneapolis College of Art and Design  




## Academic  

### 2018-Current 
Assistant Professor in the Department of Art  
Kansas State University, Manhattan, Kansas  


### 2017-2018
Visiting Assistant Professor in Studio Art  
Oberlin College and Conservatory, Oberlin, Ohio  


### 2015-2018 
Full-time Lecturer in Sculpture + Expanded Media and Foundation  
Cleveland Institute of Art, Cleveland, Ohio   
 

### 2015 
Visiting Lecturer in Sculpture - Leading theory seminars  
Cranbrook Academy of Art, Bloomfield Hills, Michigan    
